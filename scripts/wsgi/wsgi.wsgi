"""
WSGI config for pippo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os
import sys
import site

site.addsitedir('/var/www/data/django/lib/python3.5/site-packages')

sys.path.append('/var/www/data/biabar')
sys.path.append('/var/www/data/biabar/biabar')

os.environ['DJANGO_SETTINGS_MODULE'] = 'biabar.settings'

activate_env = os.path.expanduser('/var/www/data/django/bin/activate_this.py')

#execfile(activate_env, dict(__file__=activate_env))
#exec(open(activate_env).read(),dict(__file__=activate_env))
#exec(compile(open(activate_env).read(), activate_env, 'exec'))

exec(open(activate_env).read(),dict(__file__=activate_env))

#import django.core.handlers.wsgi
#application = django.core.handlers.wsgi.WSGIHandler()
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "biabar.settings")

application = get_wsgi_application()
