
activate_this = '/var/www/data/flask2/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

import sys
import os
import site

sys.path.insert(0,'/var/www/data/rave')

os.chdir('/var/www/data/rave')

from rave import app as application
