function onError(jqXHR, exception) {
    if (jqXHR.status == 0) {
        alert('Not connect. Verify Network.') ;
    } else if (jqXHR.status == 404) {
        alert('Requested page not found. [404]') ;
    } else if (jqXHR.status == 500) {
        alert('Internal Server Error [500].') ;
    } else if (exception === 'parsererror') {
        alert('Requested JSON parse failed.') ;
    } else if (exception === 'timeout') {
        alert('Time out error.') ;
    } else if (exception === 'abort') {
        alert('Ajax request aborted.') ;
    } else {
        alert('Uncaught Error.' + jqXHR.responseText ) ;
    }
}


document.getElementById('sec').disabled=true;

var $results = $('#output');
const sitePrefix = "http://site1724.tw.cs.unibo.it/wsgi/wsgi.py/page/"; //"/wsgi/wsgi.py";

$(document).ready(function() {

    var searchh = ' ';

$.ajax({
    url: 'https://en.wikipedia.org/w/api.php',
    dataType: 'jsonp',
    data: {
        action: 'query',
        format: 'json',
        prop: 'extracts',
        exchars: '200',
        exlimit: 'max',
        explaintext: '',
        exintro: '',
        pilimit: 'max',
        rawcontinue: '',
        generator: 'search',
        gsrsearch: searchh+'incategory:"Prime ministers of Italy"',
        gsrnamespace: '0',
        gsrlimit: '20'

    },

    success: function(data) {
        $results.empty();
        var pages = data.query.pages;
        console.log(pages);
        for (var page in pages) {
            var title = pages[page].title.replace(/%20/g,"_");
            $results.append(
                '<a href="/wsgi/wsgi.py/page/' + title + '" target="_blank">' +
                '<article id="result">' +
                '<h2>' + pages[page].title + '</h2>' +
                '<p>' + pages[page].extract + '</p>' +
                '</article>' +
                '</a>'
            );
        }
    },
    error: function(jqXHR, exception) {
        onError(jqXHR, exception) ;
    }
});

});

$(function () {



    const sitePrefix = "http://site1724.tw.cs.unibo.it/wsgi/wsgi.py/page/"; //"/wsgi/wsgi.py";

    $('input').keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

  var $search = $('#searchTerm');
  var $results = $('#output');



  $search.keyup (function() {

    var searchQuery = $search.val();

    $.ajax({
      url: 'https://en.wikipedia.org/w/api.php',
      dataType: 'jsonp',
      data: {
        action: 'query',
        format: 'json',
        prop: 'extracts',
        exchars: '200',
        exlimit: 'max',
        explaintext: '',
        exintro: '',
        pilimit: 'max',
        rawcontinue: '',
        generator: 'search',
        gsrsearch: searchQuery,
        gsrnamespace: '0',
        gsrlimit: '20',

      },

      success: function(data) {
        $results.empty();
        var pages = data.query.pages;
        console.log(pages);
        for (var page in pages) {
          var title = pages[page].title.replace(/%20/g,"_");
          $results.append(
            '<a href="/wsgi/wsgi.py/page/' + title + '" target="_blank">' +
              '<article id="result">' +
                '<h2>' + pages[page].title + '</h2>' +
                '<p>' + pages[page].extract + '</p>' +
              '</article>' +
            '</a>'
          );
        }
      },
        error: function(jqXHR, exception) {
            onError(jqXHR, exception) ;
        }
    });
  });
});
/*
$("h2").css("background-color","red");

<h2 style="color:red">This is a paragraph.</h2>
*/



