# -*- coding: utf-8 -*-!
import os
import tempfile
from flask import Flask
from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship, backref

import os
import time
import datetime
import logging
from flask_login import current_user

#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(tempfile.gettempdir(), 'rbase.db')
engine = create_engine('sqlite:///' + os.path.join(tempfile.gettempdir(), 'rbase.db'), echo=True)

logger = logging.getLogger(__name__)
Base = declarative_base()


########################################################################
class User(Base):
    """"""
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String)
    email = Column(String)
    password = Column(String)

    # ----------------------------------------------------------------------
    def __init__(self, username, email, password):
        """"""
        self.username = username
        self.email = email
        self.password = password
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def GetMail(self):
        mail = str(self.email)
        return mail
    def get_username(self):
        return self.username




# create tables
Base.metadata.create_all(engine)



