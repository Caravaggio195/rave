import os,tempfile
from flask import Flask

from rave import app

from flask import  render_template, request, logging, redirect, url_for,g,jsonify

from flask_login import login_user, logout_user, current_user, login_required, LoginManager

from .models import *

from .annotetor import *
from flask.ext.login import LoginManager

from sqlalchemy.orm import sessionmaker

logger = logging.getLogger(__name__)
login_manager = LoginManager() # used by flask login to handle login
login_manager.init_app(app)
login_manager.login_view = 'login'




app.secret_key = 'secretkeyhereplease'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(tempfile.gettempdir(), 'rbase.db')

################################init database##########################################
Session = sessionmaker(bind=engine)
session = Session()

user = User("admin", "admin@admin", "admin")
session.add(user)

user = User("python", "python@python", "python")
session.add(user)

user = User("ciao", "ciao@ciao", "ciao")
session.add(user)
# commit the record the database
session.commit()

session.commit()
#######################################################################################

@app.before_request
def before_request():
    """before every request handling sets the global object g.user to curr_usr if present"""
    if current_user:
        g.user = current_user


@login_manager.user_loader
def load_user(email):
    """ used by flask-login to load an User instance from an unicode id"""
    user = None
    Session = sessionmaker(bind=engine)
    s = Session()
    query = s.query(User).filter(User.id == email)
    user = query.first()
    return user

#prova varie
#from flask import render_template
@app.route('/')
def root():
    return '<h1> ciao hai sbagliato link <a href="/wsgi/wsgi.py/home">clicca qui</a></h1>'


@app.route('/home',methods=['GET','POST'])
def home():
    """renders home in reader mode"""
    return render_template('base.html', title="home", cerca=True)

@app.route('/search/<string>',methods=['GET'])
def search(string):
    return render_template('home',search=True)

@app.route('/page/<str>')
def page1(str):

    return render_template("base.html",title="page",page=True)

@app.route('/page/<str>/<str1>')
def page(str,str1):

    return render_template("base.html",title="page",page=True)

@app.route('/register',methods=['POST','GET'])
def register():
    error = None
    if request.method == 'POST':
        POST_USERNAME = str(request.form['username'])
        POST_EMAIL = str(request.form['email'])
        POST_PASSWORD = str(request.form['password'])
        Session = sessionmaker(bind=engine)
        s = Session()

        query = s.query(User).filter(User.email == POST_EMAIL)
        result = query.first()
        if result:

            error = 'email già presente'

        else:

            adduser = sessionmaker(bind=engine)
            session = adduser()
            user = User(POST_USERNAME, POST_EMAIL, POST_PASSWORD)
            session.add(user)

            # commit the record the database
            session.commit()
            session.commit()
            return redirect('login')

    """renders register in reader mode"""
    return render_template('login.html', title="register", register=True, error=error)

@app.route('/login', methods=['POST','GET'])
def login():
    error=None
    if request.method =='POST':
        """ prelevo dalla form"""
        # POST_USERNAME = str(request.form['username'])
        POST_EMAIL = str(request.form['email'])
        POST_PASSWORD = str(request.form['password'])
        """faccio la query per vedere se nel database c'e' corrispondenza"""
        Session = sessionmaker(bind=engine)
        s = Session()
        query = s.query(User).filter(User.email == POST_EMAIL, User.password == POST_PASSWORD)
        result = query.first()

        if result:
            login_user(result)
            tok=generate_token(str(g.user.get_id()))
            return redirect(url_for('home'))
        else:
             error='user o password errata, riprova'
    return render_template('login.html', login=True, error=error)
    
@app.route('/token')
@login_required
def token():
    tok = generate_token(str(g.user.get_id()))
    return tok

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))
