from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

import ast

from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework import serializers
from rest_framework.fields import empty

from annotator.models import Range, Annotation, Permissions


class PermissionsField(serializers.CharField):
    def run_validation(self, data=empty):
        if isinstance(data, list):
            data = str(data)
        return super(PermissionsField, self).run_validation(data)

    def to_representation(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)


class PermissionsSerializer(serializers.ModelSerializer):
    read = PermissionsField(
        required=False,
        allow_blank=True,
        allow_null=True
    )
    update = PermissionsField(
        required=False,
        allow_blank=True,
        allow_null=True
    )

    delete = PermissionsField(
        required=False,
        allow_blank=True,
        allow_null=True,
        source="_delete"
    )

    class Meta:
        model = Permissions
        exclude = ("id", "_delete")


class RangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Range
        exclude = ("annotation", "id")


class AnnotationSerializer(WritableNestedModelSerializer):
    user = serializers.CharField(required=False)
    ranges = RangeSerializer(many=True)
    permissions = PermissionsSerializer()

    class Meta:
        model = Annotation
        fields = "__all__"
