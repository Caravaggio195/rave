"""
this is a slightly modified version of django-annotator
(https://github.com/PsypherPunk/django-annotator) by
Roger G. Coram licensed under the Apache License 2.0
"""
from __future__ import unicode_literals

import ast
import uuid

from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.deletion import CASCADE


def get_username_max_length():
    """
    returns the max allowed length of the username allowed by the user
    model if any, otherwise sets it to 128
    """
    user_model = get_user_model()
    try:
        username_field = user_model._meta.get_field("username")
    except models.FieldDoesNotExist:
        return 128
    return username_field.max_length


class AnnotatorPermissionsField(models.TextField):
    description = "Stores annotator.js permissions"

    def __init__(self, *args, **kwargs):
        super(AnnotatorPermissionsField, self).__init__(*args, **kwargs)

    def from_db_value(self, value, expression, connection, context):
        if not value:
            return None

        return ast.literal_eval(value)

    def to_python(self, value):
        if not value:
            return None

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return ""

        return str(value)


class Permissions(models.Model):
    """
    Simple model to store annotator permissions
    :param update: list of user allowed to update (modify) the annotation,
    empty if can be edited by anyone
    :param read: list of user allowed to read (view) the annotation,
    empty if can be viewed by anyone
    :param delete: list of user allowed to delete the annotation, empty if
    it can be deleted by anyone
    """
    read = AnnotatorPermissionsField(blank=True, null=True)
    _delete = AnnotatorPermissionsField(blank=True, null=True)
    update = AnnotatorPermissionsField(blank=True, null=True)


class Annotation(models.Model):
    """
    Follows the `Annotation format
    <http://docs.annotatorjs.org/en/v1.2.x/annotation-format.html>`_,
    of ``annotatorjs``.

    :param annotator_schema_version: schema version: default v1.0
    :param created: created datetime
    :param updated: updated datetime
    :param text: content of annotation
    :param quote: the annotated text
    :param uri: URI of annotated document
    :param user: user id of annotation owner
    :param consumer: consumer key of backend
    :param permissions: the permissions associated with this annotation
    """
    # TODO ripristina vecchi valori
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    # id = models.CharField(primary_key=True, max_length=100, unique=True,
    #                  default=uuid.uuid4)
    annotator_schema_version = models.CharField(max_length=8, default="v1.0")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    text = models.TextField()
    quote = models.TextField()
    uri = models.CharField(max_length=4096, blank=True)
    user = models.CharField(max_length=get_username_max_length(), blank=True)
    # user = models.CharField(max_length=256, blank=True)
    consumer = models.CharField(max_length=64, blank=True)
    permissions = models.OneToOneField(Permissions, on_delete=models.CASCADE)

    def delete(self, *args, **kwargs):
        self.permissions.delete()
        return super(Annotation, self).delete(*args, **kwargs)

    class Meta:
        ordering = ("created",)


class Range(models.Model):
    """
    Follows the `Annotation format
    <http://docs.annotatorjs.org/en/v1.2.x/annotation-format.html>`_,
    of ``annotatorjs``.

    :param start: (relative) XPath to start element
    :param end: (relative) XPath to end element
    :param startOffset: character offset within start element
    :param endOffset: character offset within end element
    :param annotation: related ``Annotation``
    """
    start = models.CharField(max_length=128)
    end = models.CharField(max_length=128)
    startOffset = models.IntegerField()
    endOffset = models.IntegerField()
    annotation = models.ForeignKey(
        Annotation,
        related_name="ranges",
        on_delete=CASCADE
    )
