from __future__ import unicode_literals

from django.conf.urls import url

from annotator import views

urlpatterns = [
    url(r"^$", views.AnnotationRootView.as_view(), name="root"),
    url(r"^annotations/?$",views.AnnotationIndexCreateView.as_view(),name="index_create"),
    url(r"^annotations/(?P<pk>.+)/?$",views.AnnotationReadUpdateView.as_view(),name="read_update_delete"),
    url(r"^search/?$", views.AnnotationSearchView.as_view(), name="search"),
]
