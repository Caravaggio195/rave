from __future__ import unicode_literals

import json

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import (
    HttpResponse,
    HttpResponseForbidden,
    HttpResponseBadRequest
)
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View

import annotator
from annotator import models
from annotator.serializers import AnnotationSerializer


class AnnotationRootView(View):
    def get(self,second):
        return JsonResponse({
            "name": getattr(
                settings, "ANNOTATOR_NAME",
                "django-annotator-store"
            ),
            "version": annotator.__version__
        })


@method_decorator(csrf_exempt, name="dispatch")
class AnnotationIndexCreateView(View):
    def get(self, request, *args, **kwargs):
        annotations = models.Annotation.objects.all()
        serializer = AnnotationSerializer(annotations, many=True)
        return JsonResponse(serializer.data, status=200)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        data["user"] = request.user.username
        serializer = AnnotationSerializer(data=data)
        if serializer.is_valid():
            annotation = serializer.save()
            response = HttpResponse(status=303)
            response["Location"] = reverse(
                "read_update_delete",
                kwargs={"pk": annotation.id}
            )
            return response
        else:
            return HttpResponseBadRequest(content=str(serializer.errors))


@method_decorator(csrf_exempt, name="dispatch")
class AnnotationReadUpdateView(View):
    @staticmethod
    def _is_valid_permission(annotation, requested, user):
        valid_users = getattr(annotation.permissions, requested, None)
        return not valid_users or user in valid_users

    def get(self, request, *args, **kwargs):
        annotation = get_object_or_404(models.Annotation, id=kwargs["pk"])

        if AnnotationReadUpdateView._is_valid_permission(
                annotation,
                "read",
                request.user.username
        ):
            serializer = AnnotationSerializer(annotation)
            response = JsonResponse(serializer.data, status=200)
            return response

        return HttpResponseForbidden()

    def put(self, request, *args, **kwargs):
        annotation = get_object_or_404(models.Annotation, id=kwargs["pk"])
        if AnnotationReadUpdateView._is_valid_permission(
                annotation,
                "update",
                request.user.username
        ):
            data = json.loads(request.body.decode('utf-8'))
            # never trust annotator.ident
            data["user"] = request.user.username
            serializer = AnnotationSerializer(annotation, data=data)
            if serializer.is_valid():
                serializer.save()
                response = HttpResponse(status=303)
                response["Location"] = reverse(
                    "read_update_delete",
                    kwargs={"pk": serializer.data["id"]}
                )
                return response
            else:
                return HttpResponseBadRequest(content=str(serializer.errors))

        return HttpResponseForbidden()

    def delete(self, request, *args, **kwargs):
        annotation = get_object_or_404(models.Annotation, id=kwargs["pk"])
        if AnnotationReadUpdateView._is_valid_permission(
                annotation,
                "_delete",
                request.user.username
        ):
            annotation.delete()
            return HttpResponse(status=204)

        return HttpResponseForbidden()


class AnnotationSearchView(View):
    def get(self, request, *args, **kwargs):
        query = {k: v for k, v in request.GET.items()}
        annotations = models.Annotation.objects.filter(
            user__exact=request.user.username,
            **query
        ) | models.Annotation.objects.filter(
            permissions__read__contains="'{}'".format(request.user.username),
            **query
        ) | models.Annotation.objects.filter(
            permissions__read__exact="", **query
        )

        serializer = AnnotationSerializer(annotations, many=True)
        return JsonResponse({
            "total": len(serializer.data),
            "rows": serializer.data
        })
