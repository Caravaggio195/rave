# -*- coding: utf-8 -*-!

""" User Management """
from __future__ import unicode_literals
from django.contrib.auth import login, logout
from django.http import JsonResponse
from django.shortcuts import redirect
import requests
from biabar.form import LoginForm, RegisterForm
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth import login, logout
from django.http import JsonResponse
from django.shortcuts import redirect

def loginuser(request):
    """page user"""
    return render(
        request=request,
        template_name="login.html",
        context={
            "login_form": LoginForm(prefix="login")
        }

    )

def loginu(request):
    """log user """
    # just need user validation so pass None as request
    form = LoginForm(None, request.POST)

    response = JsonResponse(
        {"errors": {"": ["This form requires a POST request"]}},
        status=405
    )
    if form:
        if form.is_valid():
            login(request, form.get_user())
            response = JsonResponse({"result": "Logged in"}, status=200)

        else:  # invalid
            errors = {}

            for e in form.errors:
                errors[e] = str(form[e].errors)

            for err in form.non_field_errors():
                errors["login"] = str(err)

            response = JsonResponse({"errors": errors}, status=400)
    return response


def logoutu(request):
    """out """
    logout(request)
    return redirect("/wsgi/wsgi.wsgi/home/")

def registeruser(request):
    """page new user"""
    return render(
        request=request,
        template_name="register.html",
        context={
            "register_form": RegisterForm(prefix="reg"),

        }

    )


def register(request):
    """new user"""
    form = RegisterForm(request.POST or None)
    response = JsonResponse(
        {"errors": {"": ["This form requires a POST request"]}},
        status=405
    )
    if request.method == "POST":
        if form.is_valid():
            user = form.save()
            if (user):
                login(request, user)
                response = JsonResponse({"result": "User created"}, status=201)
            else:
                form.add_error("username", "Couldn't create user, please "
                                           "check username")
                response = JsonResponse(
                    {"errors": {"username": form["username"].errors}},
                    status=400
                )
        else:  # invalid
            errors = {}

            for e in form.errors:
                errors[e] = str(form[e].errors)

            response = JsonResponse({"errors": errors}, status=400)
    return response
