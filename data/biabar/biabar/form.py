from __future__ import (
    absolute_import, division, print_function, unicode_literals
)
from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.forms.utils import ErrorList


class RegisterForm(UserCreationForm):
    helper = FormHelper()
    helper.form_tag = False

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.error_class = ErrorListBootstrap

    def clean_username(self):
        user_model = get_user_model()
        username = self.cleaned_data["username"]
        try:
            user_model.objects.get(username__iexact=username)
        except user_model.DoesNotExist:
            return username
        raise forms.ValidationError(
            "This username is already taken.",
            code="username_taken"
        )


class LoginForm(AuthenticationForm):
    helper = FormHelper()
    helper.form_tag = False

    def __init__(self, request=None, *args, **kwargs):
        super(LoginForm, self).__init__(request, *args, **kwargs)
        self.error_class = ErrorListBootstrap
        self.fields.keyOrder = ["username", "password"]

    def clean_username(self):
        user_model = get_user_model()
        username = self.cleaned_data["username"]
        try:
            user_model.objects.get(username__iexact=username)
        except user_model.DoesNotExist:
            raise forms.ValidationError(
                "This user doesn't exists.",
                code="username_not_registered"
            )
        return username

    def clean_password(self):
        username = self.cleaned_data.get("username", None)
        password = self.cleaned_data["password"]

        if username and authenticate(
                username=username,
                password=password
        ) is None:
            raise forms.ValidationError(
                "Failed login please check password",
                code="wrong_password"
            )
        return password


class ErrorListBootstrap(ErrorList):
    def __str__(self):
        return self.as_simple_list()

    def as_list(self):
        if not self:
            return ""
        return "".join(["<li>{}</li>".format(e) for e in self])

    def as_simple_list(self):
        if not self:
            return ""
        return "".join(["{}</br>".format(e) for e in self])
