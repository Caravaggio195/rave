function onError(jqXHR, exception) {
    if (jqXHR.status == 0) {
        alert('Not connect. Verify Network.') ;
    } else if (jqXHR.status == 404) {
        alert('Requested page not found. [404]') ;
    } else if (jqXHR.status == 500) {
        alert('Internal Server Error [500].') ;
    } else if (exception === 'parsererror') {
        alert('Requested JSON parse failed.') ;
    } else if (exception === 'timeout') {
        alert('Time out error.') ;
    } else if (exception === 'abort') {
        alert('Ajax request aborted.') ;
    } else {
        alert('Uncaught Error.' + jqXHR.responseText ) ;
    }

}
document.getElementById('pri').disabled=false;
document.getElementById('sec').disabled=true;

$(document).ready(function() {

    var url = window.location.pathname;
    var res=url.replace(/\/wsgi\/wsgi.wsgi\/page\//g, "");
    var serchterm;
    serchterm = res.substr(0, res.length - 1);
    serchterm=serchterm.replace(/%20/g, "_");







    console.log(serchterm);
    const sitePrefix = "http://site1724.tw.cs.unibo.it/wsgi/wsgi.wsgi/page/";

    const CategoryUrl = 'https://en.wikipedia.org/w/api.php?action=query&prop=categories&cllimit=max&format=json&formatversion=2&titles='




    $.ajax({
        type: 'GET',
        url: "https://en.wikipedia.org/w/api.php",
        data: {
            format: "json",
            action: "parse",
            page: serchterm,
            prop :"text",
        },
        contentType: "application/json; charset=utf-8",
        dataType: 'jsonp',
        success: function(data){

            console.log(data);
            var markup = data.parse.text["*"];
            var i = $('<div></div>').html(markup);

           i.find('a').attr("class","cambiahref");

            i.find('tbody').remove();

            // remove cite error
            i.find('.mw-ext-cite-error').remove();

            i.find('.mw-editsection').remove();

            i.find('.thumbimage').remove();

            i.find('.thumbcaption').remove();

            i.find('small').remove();

            i.find('div.metadata.plainlinks.plainlist.mbox-small').remove();

            i.find('.infobox.sisterproject.noprint').remove();

            i.find('.infobox.sisterproject.plainlinks.haudio').remove();



            $('#article').html($(i));

            var old = $('.cambiahref');
            var cont;
            for (cont = 0; cont < old.length; cont++) {

            var oldUrl = $(old[cont]).attr("href");
            var newUrl = oldUrl.replace("wiki", "wsgi/wsgi.wsgi/page")+"/";
            $(old[cont]).attr("href", newUrl);

            }

        },
        error: function(jqXHR, exception) {
            onError(jqXHR, exception) ;
        }
    });



    $.ajax({
        type: 'GET',
        url: CategoryUrl + serchterm,
        dataType: 'jsonp',
        pageTitle: serchterm,
        success: function (response) {
            var Categoric = false;
            for (var i = 0; i < response.query.pages[0].categories.length; i++) {
                if (response.query.pages[0].categories[i].title === 'Category:Prime Ministers of Italy')
                    Categoric = true;
            }



            if (Categoric) {


          //      alert('fa parte della categoria!');

                $.ajax({
                    type: 'GET',
                    url: 'http://api.crossref.org/works?query=%22' + serchterm + '&sort=relevance',
                    dataType: 'json',
                    success: function (cos) {
                        for (var i = 0; i < cos.message.items.length; i++) {
                            $("#article").append(
                                '<a href="' + cos.message.items[i].URL + '" target="_blank">' +
                                '<ul id="coss' + i + '">' +
                                '<li id="cane">' + cos.message.items[i].title +
                                '  Doi:' + cos.message.items[i].DOI +
                                '  Publisher:' + cos.message.items[i].publisher + '</li>' +
                                '</ul>' +
                                '</a>'
                            );

                        }
                    }

                });

               /* var cssprimo = $('link[href="style.css"]');

                var secondo = $('link[href="secondo.css"]');

                cssprimo.replaceWith('secondo');*/

                    document.getElementById('pri').disabled=false;
                    document.getElementById('sec').disabled=true;
            }
            else{

                document.getElementById('pri').disabled=true;
                document.getElementById('sec').disabled=false;
               // alert('non fa parte della categoria!');
            }
        },
        error: function(jqXHR, exception) {
            onError(jqXHR, exception) ;
        }
    });


});











