const sitePrefix = "/wsgi/wsgi.wsgi/api";
var app = new annotator.App();
app.include(annotator.storage.http, { prefix: sitePrefix});
var id_page = $('#id_page').attr('value');
var id_revision = $('#id_revision').attr('value');
var title = $('#title').attr('value');

app.include(function () {
    return {
        beforeAnnotationCreated: function (ann) {
            //add the page id, revision id and page title to the freshly created annotation, before sending it
            ann.page_id = id_page;
            ann.revision_id = id_revision;
            ann.page_title = title;
        }
    }
});



app.include(annotator.ui.main, {element: document.body});

app.start().then(function(){
    app.annotations.load({page_id:id_page, revision_id: id_revision});

});


/*




*/