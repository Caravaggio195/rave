var _t = annotator.util.gettext;

var wikirefAnnotator = {

    sanitizeTags: function sanitizeTags(text) {
        // same as annotator.js original function, just don't escape slash
        var ESCAPE_MAP = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#39;"
        };

        return String(text).replace(/[&<>"']/g, function (c) {
            return ESCAPE_MAP[c];
        });
    },

    render: function (annotation) {
        // regular expression adapted from https://gist.github.com/ryansmith94/0fb9f6042c1e0af0d74f
        if (annotation.text) {
            var wikiUrlPattern = /(?:(?:http?:)?\/\/)+site1724\/tw\/cs\/unibo\/it\/wsgi\/\/wsgi.wsgi\/\/page\/[^\s]+/ig;

            console.log("wikiUrlPattern=" + wikiUrlPattern);

            var escapedText = wikirefAnnotator.sanitizeTags(annotation.text);
            escapedText = escapedText.replace(wikiUrlPattern, function (url) {    // create links
                var protocolPattern = /^(?:(?:http?:)?\/\/)|(?:\/page\/)/i;
                var href = protocolPattern.test(url) ? url : "http://" + url;  // add protocol if not specified or not a relative path
                return "<a href='" + href + "' target='_blank'>" + url + "</a>";
            });
            escapedText += "<div class=annotator-author> by " + (annotation.user ? annotation.user : "Anonymous") + "</div>";
            return escapedText;
        } else {
            return "<i>" + _t("No comment") + "</i>";
        }
    },

    viewerExtension: function (viewer) {
        viewer.setRenderer(wikirefAnnotator.render);
    },

    getAnnotationQuoteText: function (annotation) {
        var quote = [];
        for (var i = 0, len = annotation.ranges.length; i < len; i++) {
            var range = xpathRange.toRange(annotation.ranges[i].start,annotation.ranges[i].startOffset,annotation.ranges[i].end,annotation.ranges[i].endOffset,annRootElement);
            quote.push(String(range).trim());
            return quote.join(" / ");
        }
    },

    getAppExtension: function () {
        return {
            promptForInvalidAnnotations: function (invalidAnns) {
                // asks the user what to do about his invalid annotations
                var choice = window.confirm("There are " + invalidAnns
                        .length + " wrongly positioned annotations beloging to " +
                    "you, do you want to show them so you can fix/delete " +
                    "them?");

                if (!choice) {
                    for (var i = 0, len = invalidAnns.length; i < len; i++) {
                        this.hideAnnotation(invalidAnns[i]);
                    }
                }
            },

            addClassToHighlights: function (annotation, _class) {
                for (var i = 0, len = annotation._local.highlights.length; i < len; i++) {
                    $(annotation._local.highlights[i]).addClass(_class);
                }
            },

            hideAnnotation: function (annotation) {
                for (var i = 0, len = annotation._local.highlights.length; i < len; i++) {
                    var h = annotation._local.highlights[i];
                    console.log("annotation local "+ h );
                    if (h.parentNode !== null) {
                        $(h).replaceWith(h.childNodes);
                    }
                }
            },

            annotationsLoaded: function (annotations) {
                var invalidAnnotations = [];
                console.log('annotatoion load invalid annotatio  = '+ invalidAnnotations );
                for (var i = 0, len = annotations.length; i < len; i++) {

                    var quote = wikirefAnnotator.getAnnotationQuoteText(annotations[i]);
                    console.log("quote="+quote);
                    if (quote.toLowerCase() !== annotations[i].quote.toLowerCase()){
                        console.log(annotations[i].id + " is invalid");
                        this.addClassToHighlights(annotations[i], "annotator-hl-invalid");

                        if (annotations[i].user === app.ident.identity){
                            invalidAnnotations.push(annotations[i]);

                        }
                        else
                            this.hideAnnotation(annotations[i]);
                    }
                }
                if (invalidAnnotations.length) {
                    this.promptForInvalidAnnotations(invalidAnnotations);
                }
            },

        }
    }
};
