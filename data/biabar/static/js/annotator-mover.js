"use strict";

var _t = annotator.util.gettext;

var annotatorMover = {

    getEditorExtension: function getEditorExtension(options) {
        if (typeof (options.app) === "undefined" || options.app === null) {
            console.warn(_t("You need to provide an instantiated annotator app instance to use this module"));
            return;
        }

        return function editorExtension(editor) {
            annotatorMover.element = options.element || document.body;
            annotatorMover.appInstance = app;
            editor._original_load = editor.load;
            editor.load = annotatorMover.load;
            editor.moverAdded = false;
        }
    },

    outImage: "data:image/png;base64,iVB" +
    "ORw0KGgoAAAANSUhEUgAAABIAAAASCAQAAAD8x0bcAAAAbElEQVR4AY2S0Q" +
    "0AIQhD+WQsR2EUxrpPNrtrTGOiJkfpj9FHioBd4VATbg/kHfJCwDrkFxuWV" +
    "hMpnMYNOC9zQskkP4z4EFZQMIGmRGgRKy1oDewstxZU+xcESLATChdaIDV" +
    "TH4s+YGFVhKX7APWLUxlmYHWEAAAAAElFTkSuQmCC",
    hoverImage: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYA" +
    "AABWzo5XAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAc" +
    "dvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTczbp9jAAAAvUlEQVQ4T62R" +
    "wQ0CMQwE70lZlEIplMWTzowHZaONkwsPztJI0dqew+KIiF/cGqteZxkaCF6NrWwZNiRRbW" +
    "XLMKkS1alsChr35Jm8ExVvMnrTTg34mg+yqOKtnJnhl/VHonN84ZHwS4C3cmaGM9WQhNIJ" +
    "vijI6OnkLqsSL4arSAKvr+xSEVxympCMQV9kCVzMTJeAGoLG33+/w6CfQOlk/1BnCho6s9" +
    "ZwjjMFRpWdSmAZGpJtJbAMCwi2kog4PhjYG4kYSg/+AAAAAElFTkSuQmCC",

    load: function load(annotation, position) {
        this.annotation = annotation;

        for (var i = 0, len = this.fields.length; i < len; i++) {
            var field = this.fields[i];
            field.load(field.element, this.annotation);
        }

        if (!this.moverAdded) {
            $(this.element).find(".annotator-controls").first().prepend("<a" +
                " href='moveme' class='annotator-move'><img" +
                " class='annotator-move-icon' src=" + annotatorMover.outImage +
                " alt='move annotation'>"+"Move"+"</a>");

            $(".annotator-move")
                .on("mouseenter", function () {
                    $(".annotator-move-icon").attr("src", annotatorMover.hoverImage);
                })
                .on("mouseleave", function () {
                    $(".annotator-move-icon").attr("src", annotatorMover.outImage);
                })
                .on("click", function (evt) {
                    annotatorMover._onMoveClick(evt);
                });

            this.moverAdded = true;
        }

        else {
            $(".annotator-move-icon").attr("src", annotatorMover.outImage);
        }

        if (this.annotation.text) {
            annotatorMover.movingAnnotation = this.annotation;
            $(".annotator-move").show();
        }

        else {
            $(".annotator-move").hide();
        }

        var self = this;
        return new Promise(function (resolve, reject) {
            self.dfd = { resolve: resolve, reject: reject };
            self.show(position);
        });
    },

    _onMoveClick: function (evt) {
        evt.preventDefault();

        if (this.moverTextSelector) {
            this.moverTextSelector.destroy();
            delete this.moverTextSelector;
        }
        if (this.banner) {
            this.banner.close();
        }
        this.banner = annotator.notification.banner(_t("Select the new range to move the annotation to, then click save"));
        var self = this;

        this.moverTextSelector = new annotator.ui.textselector.TextSelector(
            this.element, {
                onSelection: function (ranges, event) {
                    $(".annotator-adder").addClass("annotator-hide");
                    if (ranges.length > 0) {
                        self.movingAnnotation.text = $(".annotator-widget textarea").val();
                        var text = [],
                            serializedRanges = [];

                        for (var i = 0, len = ranges.length; i < len; i++) {
                            var r = ranges[i];
                            text.push(r.text().trim());
                            serializedRanges.push(r.serialize(self.element, ".annotator-hl"));
                        }

                        self.movingAnnotation.quote = text.join(" / ");
                        self.movingAnnotation.ranges = serializedRanges;

                        self.banner.close();
                        self.appInstance.annotations.update(self.movingAnnotation);
                    }

                    else {
                        self.banner.close();
                        self.banner = annotator.notification.banner(_t("Move aborted: no range selected"));
                    }
                    self.moverTextSelector.destroy();
                    delete self.moverTextSelector;
                }
            });
    }
};
