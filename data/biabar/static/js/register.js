function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    var host = document.location.host;
    var protocol = document.location.protocol;
    var sr_origin = "//" + host;
    var origin = protocol + sr_origin;
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||

        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"));
        }
    }
});


function registerUser() {
    $.ajax({
        url: "/wsgi/wsgi.wsgi/register/",
        type: "POST",
        data: {
            username: $("#id_reg-username").val(),
            password1: $("#id_reg-password1").val(),
            password2: $("#id_reg-password2").val(),
            email: $("#id_reg-email").val()
        },
        success: function () {
                setTimeout(function () {
                location.href = "http://site1724.tw.cs.unibo.it/wsgi/wsgi.wsgi/home/";
            }, 1000);
        },
          error:function(error){
            console.log(error);
            alert("Wrong username or password");
            $("#register_form").trigger("reset");
            }
    });
}

function loginUser() {
    $.ajax({
        url: "/wsgi/wsgi.wsgi/login/",
        type: "POST",
        data: {
            username: $("#id_login-username").val(),
            password: $("#id_login-password").val(),
        },
        success: function (ret) {
            console.log(ret);
            setTimeout(function () {
                location.href = "http://site1724.tw.cs.unibo.it/wsgi/wsgi.wsgi/home";
            }, 1000);
        },
        error:function(error){
            console.log(error);
            alert("Wrong username or password");
            $("#login_form").trigger("reset");
            }
    });
}
$(document).ready(function () {
        $("#register_form").on("submit", function (evt) {
            evt.preventDefault();
            registerUser();
        });
        $("#login_form").on("submit", function (evt) {
        evt.preventDefault();
        loginUser();
        });
});

